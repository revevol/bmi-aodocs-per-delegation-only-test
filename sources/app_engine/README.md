# App Engine Project

This example illustrates how to create a simple project with App Engine enabled.

It will do the following:
- Create a new App Engine app

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| project\_id |  The project ID where app engine is created  | `string` | `""` | yes |
| location\_id | The location app engine is serving from | `string` | `""` | yes |

## Outputs

| Name | Description |
|------|-------------|
| app\_name | Unique name of the app, usually apps/{PROJECT\_ID}. |
| default\_hostname | The default hostname for this app. |
| location\_id | The location app engine is serving from |
| project\_id | The project ID where app engine is created |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->